package com.cleverobserver.generator.models;

import javax.persistence.*;

/**
 * Created by dido on 9/2/15.
 * This is our authentication models
 */
@Entity
public class Account {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Version
	private Long version;

	@Column(name = "username",unique = true)
	private String username;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}


	public long getId() {
		return id;
	}

}
