package com.cleverobserver.generator.repositories;

import com.cleverobserver.generator.GeneratorApplication;
import com.cleverobserver.generator.models.Account;
import com.jayway.restassured.RestAssured;
import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static com.jayway.restassured.RestAssured.when;

/**
 * Created by dido on 9/4/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = GeneratorApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class AccountRepositoryTest {

	@Autowired
	AccountRepository accountRepository;

	Account account1;

	@Value("${local.server.port}")
	private int port;

	@Before
	public void setUp() throws Exception {
		account1 = new Account();
		account1.setUsername("account1");
		accountRepository.save(account1);
		RestAssured.port = port;
	}

	@Test
	public void testFindByUsername() throws Exception {
		when().get("accounts/search/findByUsername?name={username}", account1.getUsername()).then().statusCode(HttpStatus.SC_OK)
				.body("_embedded.accounts.username", Matchers.hasItem(account1.getUsername()));
	}
}